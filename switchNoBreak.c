/* This C program demonstrates the switch statement without using breaks */
#include <stdio.h>
int main() {
    //ch is declared as +
    char ch = '+';
    //a and b are initialized and set
    int a = 10, b = 20;
    //f is initialized
    int f;
    float aa = a;
    float bb = b;
    //prints current character
    printf("ch = %c\n", ch);
    //switch statement is created to determine how the operation of a and b are created
    switch (ch) {
        //if + then add a and b
        case '+':
            f = a + b;
            printf("f = %d\n", f);
            
            //if - then subtract a and b
        case '-':
            f = a - b;
            printf("f = %d\n", f);
            
            //if * then multiply a and b
        case '*':
            f = a * b;
            printf("f = %d\n", f);
           
            //if / then divide a and b
        case '/':
            printf("f = %f\n", aa / bb);
           
            //if no other options are selected then it is an invalid operator
        default:
            printf("invalid operator\n");
            
    }
    ch = '-';
    printf("ch = %c\n", ch);
    switch (ch) {
        //if + then add a and b
        case '+':
            f = a + b;
            printf("f = %d\n", f);
            
            //if - then subtract a and b
        case '-':
            f = a - b;
            printf("f = %d\n", f);
           
            //if * then multiply a and b
        case '*':
            f = a * b;
            printf("f = %d\n", f);
            
            //if / then divide a and b
        case '/':
            printf("f = %f\n", aa / bb);
           
            //if no other options are selected then it is an invalid operator
        default:
            printf("invalid operator\n");
            
    }
    ch = '*';
    printf("ch = %c\n", ch);
    switch (ch) {
        //if + then add a and b
        case '+':
            f = a + b;
            printf("f = %d\n", f);
           
            //if - then subtract a and b
        case '-':
            f = a - b;
            printf("f = %d\n", f);
           
            //if * then multiply a and b
        case '*':
            f = a * b;
            printf("f = %d\n", f);
          
            //if / then divide a and b
        case '/':
            printf("f = %f\n", aa / bb);
           
            //if no other options are selected then it is an invalid operator
        default:
            printf("invalid operator\n");
           
    }
    ch = '/';
    printf("ch = %c\n", ch);
    switch (ch) {
             //if + then add a and b
        case '+':
            f = a + b;
            printf("f = %d\n", f);
            
            //if - then subtract a and b
        case '-':
            f = a - b;
            printf("f = %d\n", f);
            
            //if * then multiply a and b
        case '*':
            f = a * b;
            printf("f = %d\n", f);
           
            //if / then divide a and b
        case '/':
            printf("f = %f\n", aa / bb);
           
            //if no other options are selected then it is an invalid operator
        default:
            printf("invalid operator\n");
           
    }
    ch = '%';
    printf("ch = %c\n", ch);
    switch (ch) {
              //if + then add a and b
        case '+':
            f = a + b;
            printf("f = %d\n", f);
            
            //if - then subtract a and b
        case '-':
            f = a - b;
            printf("f = %d\n", f);
            
            //if * then multiply a and b
        case '*':
            f = a * b;
            printf("f = %d\n", f);
           
            //if / then divide a and b
        case '/':
            printf("f = %f\n", aa / bb);
           
            //if no other options are selected then it is an invalid operator
        default:
            printf("invalid operator\n");
    }
}