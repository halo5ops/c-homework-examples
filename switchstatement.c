/* This C program demonstrates the switch statement without using breaks. */
#include <stdio.h>
int main() {
    char ch;
    int a = 10, b = 20;
    int f;
    float aa = a;
    float bb = b;
    for(int i = 0; i < 5; i++) {
        printf("Please select an operation: \n");
        ch = getchar();
        switch (ch) {
            //if + then add a and b
            case '+':
                f = a + b;
                printf("f = %d\n", f);
                break;
                //if - then subtract a and b
            case '-':
                f = a - b;
                printf("f = %d\n", f);
                break;
                //if * then multiply a and b
            case '*':
                f = a * b;
                printf("f = %d\n", f);
                break;
                //if / then divide a and b
            case '/':
                printf("f = %f\n", aa / bb);
                break;
                //if no other options are selected then it is an invalid operator so break from operation
            default:
                printf("invalid operator\n");
                return 0;
        }
       ch = getchar();
    }
}