## Project Description
The project is a switch case example using C. The main objective of the program is to show the effectiveness of using break calls when using switch statements to ensure when a 
specific action is called it breaks out of the loop in order to not repeat or reread data. The first file Hw01Q2.c shows the failures of not using break statements while file hw1q2_2.c shows the correction if these mistakes. The other program StringCypher reads in a string and can either encrypt or decrypt it and each is stored in a 2D array.

## Running Program
In order to run program switchNoBreak.c you must first run
1. gcc switchNoBreak.c
2. ./a.out
In order to run program switchstatement.c you must first run
1. gcc switchstatement.c
2. ./a.out
In order to run program  stringcipher.c you must first run
1. gcc switchstatement.c
2. ./a.out
